# Project Gestalt
#### The creation of an artificial neural system 

> Created and maintained by Rodrigo Pereira Cruz (peroz)

## About this repository

This repository contains code related to Project Gestalt. The project is still in its early stages, but the results so far have proven promising. As of now, the gestalt network has the following properties:

* Roughly 0.615 loss on GPU with MSE loss (decreasing); <br>
* Over 810k parameters (increasing); <br>
* 1 receiver network, 10 transmitter networks and 1 gate network (increasing). <br>

The system is being trained on the MNIST dataset, owing to its simplicity, and will eventually be scaled up to deal with tougher challenges.

## About the project

Project Gestalt is a personal initiative to create a gestalt network<sup>[1](#names)</sup>, a generalist neural system or, in simple terms, a neural network made up of other neural networks. The idea behind it is to assemble several types of neural networks, such as autoencoders, GANs and transformers in a single, multifunctional and highly scalable structure that more closely resembles a human brain and can handle any machine learning task.

A gestalt network is structured as a modular, yet complex model that is made up of 3 critical components, which are: <br>  

* **Receiver network**: an encoder-based neural network whose job is to receive inputs, modify them as needed and send them to a transmitter network. Notably, there are no activation functions in a receiver network; <br>
* **Transmitter network**: a "messenger" network, which processes data coming from a receiver or another transmitter by performing dropout and applying the reLU activation function. Ideally, several transmitter networks will be present in a gestalt network and will communicate with each other; <br>
* **Gate network**: a decoder-based neural network that's responsible for handling data coming from the transmitters, applying transformations and activations as needed and producing the penultimate output. The gestalt network itself will produce the final output based on the gate network's output data.

***

<small><sup><a name="names">1</sup></a> While "gestalt network" is the preferred name for this system, other naming options include neural forest and composite neural network. </small>
